CREATE DATABASE `library` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `library`;


CREATE TABLE IF NOT EXISTS `authors`(
   `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
   `authors_id` int(10) unsigned NOT NULL,
   `authors_name` varchar(50) NOT NULL,
   `authors_age` smallint(5) unsigned NOT NULL,
   `book_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `books`(
   `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
   `book_id` int(10) unsigned NOT NULL,
   `book_name` varchar(50) NOT NULL,
   `authors_id` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

SELECT a.authors_name, count(*) 
FROM authors a, books b 
WHERE a.authors_id = b.authors_id 
GROUP BY a.authors_name 
HAVING count(*) < 7;